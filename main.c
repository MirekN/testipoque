
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <sys/queue.h>

#include <rte_memory.h>
#include <rte_memzone.h>
#include <rte_launch.h>
#include <rte_eal.h>
#include <rte_per_lcore.h>
#include <rte_lcore.h>
#include <rte_debug.h>

#include <pace2.h>

#include <rte_mbuf.h>
#include <rte_ethdev.h>
#include <rte_common.h>
#include <rte_debug.h>
#include <rte_eal.h>
#include <rte_mempool.h>
#include <rte_ethdev.h>
#include <rte_errno.h>
#include <rte_cycles.h>
#include <rte_errno.h>

//#include <linux/ip.h>
#include <arpa/inet.h>
#include <linux/udp.h>

#include <stdbool.h>
#include <getopt.h>

//-----------------------------------------------------------------------------------


#define PRINT_MAC(addr)		printf("%02"PRIx8":%02"PRIx8":%02"PRIx8 \
		":%02"PRIx8":%02"PRIx8":%02"PRIx8,	\
		addr.addr_bytes[0], addr.addr_bytes[1], addr.addr_bytes[2], \
		addr.addr_bytes[3], addr.addr_bytes[4], addr.addr_bytes[5])
		
#define UNUSED(x) (void)(x)
#define ARRAYSIZE(a) (sizeof(a) / sizeof(*(a)))

#ifndef __u32
typedef unsigned int __u32;
#endif

//-----------------------------------------------------------------------------------

struct dpdk_statistics {
    size_t numPackets;              // The number of packets received
    size_t numBytes;                       // The number of bytes received
} dpdk_stats;


struct global_options {
    uint64_t print_stats_after_x_packets;
    uint32_t port;
    bool full_features;
    bool promiscuous_mode;
} options = { .print_stats_after_x_packets = 10, .port = 0 , .promiscuous_mode = 0};


void (*packet_processing)(const uint64_t timestamp, const struct iphdr *iph, uint16_t ipsize);
int dpdk_init(int argc, char *argv[]);
int dpdk_finish(void);

//int packet_loop(packet_processing pckProc);
int packet_loop(void (*packet_processing)(const uint64_t timestamp, const struct iphdr *iph, uint16_t ipsize));
//-----------------------------------------------------------------------------------


// Memory allocation wrappers 
static void *malloc_wrapper( u64 size, int thread_ID, void *user_ptr, int scope ) 
{
	UNUSED(thread_ID);
	UNUSED(user_ptr);
	UNUSED(scope);

    return malloc( size );
} // malloc_wrapper 

static void free_wrapper( void *ptr, int thread_ID, void *user_ptr, int scope ) 
{
	UNUSED(thread_ID);
	UNUSED(user_ptr);
	UNUSED(scope);

    free( ptr );
} // free_wrapper 

static void *realloc_wrapper( void *ptr, u64 size, int thread_ID, void *user_ptr, int scope ) 
{
	UNUSED(thread_ID);
	UNUSED(user_ptr);
	UNUSED(scope);

    return realloc( ptr, size );
}

// PACE 2 configuration structure 
static struct PACE2_global_config pace2_config;

// PACE 2 module pointer 
static PACE2_module *pace2;

struct pace_statistics {
   uint64_t packet_counter;
   uint64_t byte_counter;
   uint64_t protocol_counter[PACE2_PROTOCOL_COUNT];
   uint64_t protocol_counter_bytes[PACE2_PROTOCOL_COUNT];
   uint64_t protocol_stack_length_counter[PACE2_PROTOCOL_STACK_MAX_DEPTH];
   uint64_t protocol_stack_length_counter_bytes[PACE2_PROTOCOL_STACK_MAX_DEPTH];
   uint64_t application_counter[PACE2_APPLICATIONS_COUNT];
   uint64_t application_counter_bytes[PACE2_APPLICATIONS_COUNT];
   uint64_t attribute_counter[PACE2_APPLICATION_ATTRIBUTES_COUNT];
   uint64_t attribute_counter_bytes[PACE2_APPLICATION_ATTRIBUTES_COUNT];
   uint64_t http_response_payload_bytes;

   uint64_t next_packet_id;

   uint64_t license_exceeded_packets;
} pace_stats;

//-----------------------------------------------------------------------------------

int dpdk_init(int argc, char *argv[])
{


	int rc = rte_eal_init(argc, argv);

	if (rc < 0) 
	{
//		fprintf(stderr, "rte_eal_init() failed with %d (%s)\n", rte_errno, rte_strerror(rte_errno));
		printf("rte_eal_init() failed with %d (%s)\n", rte_errno, rte_strerror(rte_errno));
		return rc;
	}

	int consumed_arguments = rc;

	const size_t number_of_elements = ((1<<10) - 1);
	struct rte_mempool *memory_pool;
	memory_pool = rte_pktmbuf_pool_create("basicInitMemoryPool", number_of_elements, 32, 0, RTE_MBUF_DEFAULT_BUF_SIZE, SOCKET_ID_ANY);
	if (!memory_pool) 
	{
//		fprintf(stderr, "rte_mempool_create() failed with %d (%s)\n", rte_errno, rte_strerror(rte_errno));
		printf("rte_mempool_create() failed with %d (%s)\n", rte_errno, rte_strerror(rte_errno));
		return -ENOMEM;
	}

//	fprintf(stdout, "The system has %d ports available.\n", rte_eth_dev_count());
	printf("The system has %d ports available.\n", rte_eth_dev_count());

	unsigned port = 0;
	const unsigned queues = 1;

	for (port = 0; port < rte_eth_dev_count(); ++port) 
	{
		static struct rte_eth_conf conf = {
				.rxmode = {
						.split_hdr_size = 0,
						.max_rx_pkt_len = 1518, /** < Packet size */
						.header_split   = 0, /**< Header Split disabled */
						.hw_ip_checksum = 0, /**< IP checksum offload disabled */
						.hw_vlan_filter = 0, /**< VLAN filtering disabled */
						.jumbo_frame    = 0, /**< Jumbo Frame Support disabled */
						.hw_strip_crc   = 0, /**< CRC stripped by hardware */
						.mq_mode = ETH_MQ_RX_RSS,
				},
				.rx_adv_conf = {
						.rss_conf = {
								.rss_key = NULL,
								.rss_hf = ETH_RSS_IP,
						}
				},
				.txmode = {
						.mq_mode = ETH_MQ_TX_NONE,
				},
		};

		struct rte_eth_dev_info info;
		rte_eth_dev_info_get(port, &info);

		struct ether_addr mac_addr;
		rte_eth_macaddr_get(port, &mac_addr);
		char mac_addr_buffer[ETHER_ADDR_FMT_SIZE];
		ether_format_addr(mac_addr_buffer, sizeof(mac_addr_buffer), &mac_addr);

		fprintf(stdout, "Port %d with mac address %s can have %d rx and %d tx queues\n", port, mac_addr_buffer,info.max_rx_queues, info.max_tx_queues);

		rc = rte_eth_dev_configure(port, queues, queues, &conf);
		if (rc) 
		{
			fprintf(stderr, "rte_eth_dev_configure() for interface %d failed with %d (%s)\n", port, rc, strerror(rc));
			return rc;
		}

		const unsigned number_of_descriptors = 128;
		unsigned queue;
		// configure rx queues 
		for (queue = 0; queue < queues; ++queue) {
			rc = rte_eth_rx_queue_setup(port, queue, number_of_descriptors, SOCKET_ID_ANY, /* default rx config*/ NULL, memory_pool);
			if (rc) 
			{
				fprintf(stderr, "rte_eth_rx_queue_setup() for queue %d on interface %d failed with %d(%s)\n", queue, port, rc, strerror(rc));
				return rc;
			}
		}
		// configure tx queues 
		for (queue = 0; queue < queues; ++queue) 
		{
			rc = rte_eth_tx_queue_setup(port, queue, number_of_descriptors, SOCKET_ID_ANY, /* default tx config*/ NULL);
			if (rc) 
			{
				fprintf(stderr, "rte_eth_tx_queue_setup() for queue %d on interface %d failed with %d(%s)\n", queue, port, rc, strerror(rc));
				return rc;
			}
		}
	}

    return consumed_arguments;
}

//-----------------------------------------------------------------------------------
int dpdk_finish(void)
{
    fprintf(stdout, "Processed %zd frames with %zd bytes.\n", dpdk_stats.numPackets, dpdk_stats.numBytes);
    return 0;
}
//-----------------------------------------------------------------------------------
// Print out all PACE 2 events currently in the event queue 
static void process_events(void)
{
    PACE2_event *event;

    while ( ( event = pace2_get_next_event( pace2, 0 ) ) ) 
    {
        // some additional processing is necessary 

        if ( event->header.type == PACE2_CLASS_HTTP_EVENT ) 
	{
            const PACE2_class_HTTP_event * const http_event = &event->http_class_meta_data;
            if (http_event->meta_data_type == PACE2_CLASS_HTTP_RESPONSE_DATA_TRANSFER) 
	    {
                const struct ipd_class_http_transfer_payload_struct * const http_payload = &http_event->event_data.response_data_transfer;
                pace_stats.http_response_payload_bytes += http_payload->data.content.length;
            }
        }
    }
} // process_events 
//-----------------------------------------------------------------------------------
static void stage3_to_5( void )
{
    const PACE2_event *event;
    PACE2_bitmask pace2_event_mask;
    PACE2_packet_descriptor *out_pd;

    // Process stage 3 and 4 as long as packets are available from stage 2 
    while ( (out_pd = pace2_s2_get_next_packet(pace2, 0)) ) 
    {

        // Account every processed packet 
        pace_stats.packet_counter++;
        pace_stats.byte_counter += out_pd->framing->stack[0].frame_length;

        // Process stage 3: packet classification 
        if ( pace2_s3_process_packet( pace2, 0, out_pd, &pace2_event_mask ) != PACE2_S3_SUCCESS ) 
	{
            continue;
        } // Stage 3 processing 

        // Get all thrown events of stage 3 
        while ( ( event = pace2_get_next_event(pace2, 0) ) ) 
	{
            // some additional processing is necessary 
            if ( event->header.type == PACE2_CLASSIFICATION_RESULT ) 
	    {
                PACE2_classification_result_event const * const classification = &event->classification_result_data;
                u8 attribute_iterator;

                pace_stats.protocol_counter[classification->protocol.stack.entry[classification->protocol.stack.length-1]]++;
                pace_stats.protocol_counter_bytes[classification->protocol.stack.entry[classification->protocol.stack.length-1]] += out_pd->framing->stack[0].frame_length;
                pace_stats.protocol_stack_length_counter[classification->protocol.stack.length - 1]++;
                pace_stats.protocol_stack_length_counter_bytes[classification->protocol.stack.length - 1] += out_pd->framing->stack[0].frame_length;

                pace_stats.application_counter[classification->application.type]++;
                pace_stats.application_counter_bytes[classification->application.type] += out_pd->framing->stack[0].frame_length;

                for ( attribute_iterator = 0; attribute_iterator < classification->application.attributes.length; attribute_iterator++) {
                    pace_stats.attribute_counter[classification->application.attributes.list[attribute_iterator]]++;
                    pace_stats.attribute_counter_bytes[classification->application.attributes.list[attribute_iterator]] += out_pd->framing->stack[0].frame_length;
                }
            } 
            else if ( event->header.type == PACE2_LICENSE_EXCEEDED_EVENT ) 
	    {
                pace_stats.license_exceeded_packets++;
            }
        } // Stage 3 event processing 

        // Process stage 4: protocol decoding 
        if (pace2_config.s4_decoding.enabled) 
	{
            if (pace2_s4_process_packet( pace2, 0, out_pd, NULL, &pace2_event_mask ) != PACE2_S4_SUCCESS) 
	    {
                continue;
            }

            // Print out decoder events 
            process_events();
        }

    } // Stage 2 packets 

    // Process stage 5: timeout handling 
    if ( pace2_s5_handle_timeout( pace2, 0, &pace2_event_mask ) != 0 ) 
    {
        return;
    }

    // Print out decoder events generated while cleaning up flows that timed out 
    if (pace2_config.s4_decoding.enabled) 
    {
        process_events();
    }

} // stage3_to_5 
//-----------------------------------------------------------------------------------
static void stage1_and_2( const uint64_t timestamp, const struct iphdr *iph, uint16_t ipsize )
{
//  int a=iph->version;
//  a+=ipsize;
//  a+=(int)timestamp;
    
  PACE2_packet_descriptor pd;

  // Stage 1: Prepare packet descriptor and run ip defragmentation 
  if (pace2_s1_process_packet( pace2, 0, timestamp, iph, ipsize, PACE2_S1_L3, &pd, NULL, 0 ) != PACE2_S1_SUCCESS) 
  {
      return;
  }

  // Set unique packet id. The flow_id is set by the internal flow tracking 
  pd.packet_id = ++pace_stats.next_packet_id;

  // Stage 2: Packet reordering 
  if ( pace2_s2_process_packet( pace2, 0, &pd ) != PACE2_S2_SUCCESS ) 
  {
      return;
  }

  stage3_to_5();
    
} // stage1_and_2 

//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// Print classification results 
static void pace_print_results( void )
{
    u32 c;

    printf("%-20s %-15s %s\n", "Protocol", "Packets", "Bytes" );
    for ( c = 0; c < PACE2_PROTOCOL_COUNT; c++ ) 
    {
        if ( pace_stats.protocol_counter_bytes[c] != 0 ) 
    {
            printf("  %-20s %-15lu %lu\n",
                     pace2_get_protocol_long_str(c), pace_stats.protocol_counter[c], pace_stats.protocol_counter_bytes[c] );
        }
    }

    printf("\n  %-20s %-15s %s\n", "Stack length", "Packets", "Bytes" );
    for ( c = 0; c < PACE2_PROTOCOL_STACK_MAX_DEPTH; c++ ) 
    {
        printf("  %-20u %-15lu %lu\n",
                 c, pace_stats.protocol_stack_length_counter[c], pace_stats.protocol_stack_length_counter_bytes[c] );
    }

    printf("\n  %-20s %-15s %s\n", "Application", "Packets", "Bytes" );
    for ( c = 0; c < PACE2_APPLICATIONS_COUNT; c++ ) 
    {
        if ( pace_stats.application_counter_bytes[c] != 0 ) 
    {
            printf("  %-20s %-15lu %lu\n",
                     pace2_get_application_short_str(c), pace_stats.application_counter[c], pace_stats.application_counter_bytes[c] );
        }
    }

    printf("\n  %-20s %-15s %s\n", "Attribute", "Packets", "Bytes" );
    for ( c = 0; c < PACE2_APPLICATION_ATTRIBUTES_COUNT; c++ ) 
    {
        if ( pace_stats.attribute_counter[c] != 0 ) 
    {
            printf("  %-20s %-15lu %lu\n",
                     pace2_get_application_attribute_str(c), pace_stats.attribute_counter[c], pace_stats.attribute_counter_bytes[c] );
        }
    }

    printf("\nHTTP response payload bytes: %lu\n", pace_stats.http_response_payload_bytes );
    printf("\nPacket counter: %lu\n--------------------------------------------------------\n", pace_stats.packet_counter );

    if ( pace_stats.license_exceeded_packets > 0 ) 
    {
        printf("License exceeded packets: %lu.\n\n", pace_stats.license_exceeded_packets );
    }
} // pace_print_results 
//-----------------------------------------------------------------------------------
#define NW_GTPV2C_UDP_PORT                                              (2123) // UDP Port for GTP frames

typedef struct _EtherHdr
{
	u_char  ether_dst[6];
	u_char  ether_src[6];
	u_short ether_type;
} EtherHdr;

//-----------------------------------------------------------------------------------
/*
int testframe(const struct iphdr *iph, uint16_t ipsize)
{
  
  struct udphdr
}
*/
//-----------------------------------------------------------------------------------
#define NW_GTPV2C_MINIMUM_HEADER_SIZE									8    //< Size of GTPv2 minimun header  

#define GTP_MSG_CREATE_PDP_REQ      0x10
#define GTP_MSG_CREATE_PDP_RESP     0x11
 
#define ___swab16(x) \
	((u_short)( \
	(((u_short)(x) & (u_short)0x00ffU) << 8) | \
	(((u_short)(x) & (u_short)0xff00U) >> 8) ))

#define ___swab32(x) \
	((u_long)( \
	(((u_long)(x) & (u_long)0x000000ffUL) << 24) | \
	(((u_long)(x) & (u_long)0x0000ff00UL) <<  8) | \
	(((u_long)(x) & (u_long)0x00ff0000UL) >>  8) | \
	(((u_long)(x) & (u_long)0xff000000UL) >> 24) ))

#pragma GCC diagnostic push  
#pragma GCC diagnostic ignored "-Wcast-qual"			// const	
//#pragma GCC diagnostic ignored "-Wunused-variable"		// unused varaible timestamp
//#pragma GCC diagnostic ignored "-Wunused-parameter"		// unused parameters in packet_loop	


int packet_loop(void (*packet_processing)(const uint64_t timestamp, const struct iphdr *iph, uint16_t ipsize))
//int packet_loop(packet_processing pckProc)
{
  
    struct rte_mbuf *pkts_burst[32];
    const unsigned port = options.port;
    const unsigned queue = 0;

    fprintf(stdout, "Starting port %d.\n", port);

    int rc = rte_eth_dev_start(port);
    if (rc) 
    {
	    fprintf(stderr, "rte_eth_dev_start() failed with %d", rc);
	    return rc;
    }

    if (options.promiscuous_mode == 1) 
    {
        rte_eth_promiscuous_enable(port);
    }

#ifdef PRNT_OTHR    
    fprintf(stdout, "Port %d is up.\n", port);
#endif
    
   // pass each packet to PACE
    while (1) 
    {

	    int nrx = rte_eth_rx_burst(port, queue, pkts_burst, ARRAYSIZE(pkts_burst));
	    if (nrx) 
	    {
		    int i;
		    const uint64_t time_stamp = rte_get_timer_cycles();
		    const uint64_t timestamp = time_stamp * 1000 /rte_get_timer_hz();

		    for (i = 0; i < nrx; ++i) 
		    {
			    struct rte_mbuf *mbuf = pkts_burst[i];
			    const char *data;
			    data = rte_pktmbuf_mtod(mbuf, const char*);
			    unsigned len = rte_pktmbuf_pkt_len(mbuf);
#ifdef PRNT_OTHR 
			    const struct _mac {
				    uint8_t dst[6];
				    uint8_t src[6];
				    uint16_t typelen;
			    } *mac = (const struct _mac*) data;
   
			    printf("%6s %016llX - %04x - %02X:%02X:%02X:%02X:%02X:%02X  %02X:%02X:%02X:%02X:%02X:%02X  %04X\n",
					    "DPDK",	(unsigned long long)time_stamp, len,
					    mac->dst[0], mac->dst[1], mac->dst[2], mac->dst[3], mac->dst[4], mac->dst[5],
					    mac->src[0], mac->src[1], mac->src[2], mac->src[3], mac->src[4], mac->src[5],
					    mac->typelen);
#endif
			    //...........................................
			    //...........................................
			    //...........................................
			    //...........................................
			    u_char* buf=(unsigned char*)data;
			    if (((EtherHdr *)buf)->ether_type != 8) // Not eth frame 
			    {
				    continue;
			    }			    
			    len -= sizeof(EtherHdr);
			    struct iphdr* ip = (struct iphdr*)(buf + sizeof(EtherHdr));
			    //if ((ip->tos & 0xF0) != 0x40) // Di (Nie wiem czy moze byc IP6 ??? )
			    if (ip->version != 4) // not the version IP4 (maybe IP6)
			    {
				    continue;
			    }
			    if (ip->protocol == 0x11)	// UDP
			    {
				    buf = ((unsigned char*)ip) + sizeof(struct iphdr) + sizeof(struct udphdr);
				    len -= (sizeof(struct iphdr) + sizeof(struct udphdr));
			    }
			    else if (ip->protocol == 0x06) // TCP
			    {
			      continue;
			    }
			    else // not the TCP/UDP
			    {
			      continue;
			    }			    
			    
			    if (NW_GTPV2C_MINIMUM_HEADER_SIZE > len) // Header GTP to short
			    {
			      continue;
			    }
			    //..........................
			    if ((buf[0] & 0x10)==0) // Must be flag protocol GTP (bit 5)
			    {
			      continue;
			    }
			    u_char ver = (buf[0] & 0xE0);
			    if ((ver != 0x20) && (ver != 0x40)) //Must be version 1 lub 2 of GTP
			    {
			      continue;
			    }
//			    unsigned char tmp = buf[1];
			    if (buf[1] == 0xFF) // User Data
			    {  //Dane
				    unsigned len = *((unsigned short*)(buf + 2));
				    len = ___swab16(len);
		    //			tmp = (*((u_char*)(buf + 2)) << 8) + *((u_char*)(buf + 3));
				    //printf("Data length=%d\n",len);
				    u_long teid = *((u_long*)(buf + 4));
				    teid=___swab32(teid);
				    //printf("Teid=%x\n", teid);
				    //.........................................
				    ip = (struct iphdr*)(buf + 8);
				    /*
				    printf("IP Src=");		
				    PrintIP(ip->ip_src);
				    printf("\nIP Dst=");
				    PrintIP(ip->ip_dst);
				    printf("\n\n");
				    */
//printf("%zd\n",dpdk_stats.numPackets);
				    
				    (*packet_processing)(timestamp, ip, len);			    
				    //.........................................
			    }
			    else if ((ver == 0x20) && (buf[1]=GTP_MSG_CREATE_PDP_RESP)) // Control frame of GTP (veraion 1)
			    {

			    }			    
			    //...........................................
			    //...........................................
			    //...........................................
			    dpdk_stats.numPackets++;
			    dpdk_stats.numBytes += len;

			    /*  
			    if (mac->typelen == htons(0x0800) || mac->typelen == htons(0x86dd)) 
			    {
				    
				    // what the hack, the fp use a struct iphdr* and then do a cast to void* 
				    const struct iphdr *ip = (const struct iphdr *) (data + sizeof(*mac));
				    const size_t iplen = len - sizeof(*mac);
				    //.......................................................
				    // Search IP (IP SVGSN)
				    __u32 fndAddr=0x12345678;
				    if (ip->saddr == fndAddr && ip->daddr == fndAddr)
				    {
				    }
				    //.......................................................				    				    
				    //.......................................................				    
				    // hand-over packet to PACE:
				    (*packet_processing)(timestamp, ip, iplen); // -4 = CRC length 
				   
			    }
			    */

			    if (dpdk_stats.numPackets > 1 && (dpdk_stats.numPackets % options.print_stats_after_x_packets) == 0) 
			    {
				pace_print_results();  
			    }
		    }
	    }
    }

    return 0;
}
#pragma GCC diagnostic pop   
//-----------------------------------------------------------------------------------
// Configure and initialize PACE 2 module 
static void pace_configure_and_initialize( const char * const license_file )
{
    // Initialize configuration with default values 
    pace2_init_default_config_with_ticks_per_second( &pace2_config, 1000 );
    pace2_set_license_config(&pace2_config, license_file);

    // Set necessary memory wrapper functions 
    pace2_config.general.pace2_alloc = malloc_wrapper;
    pace2_config.general.pace2_free = free_wrapper;
    pace2_config.general.pace2_realloc = realloc_wrapper;

    // Set callback for additional debug informations 
    pace2_config.general.process_information = pace2_default_process_information_callback_impl;

    // set size of hash table for flow and subscriber tracking 
    // pace2_config.tracking.flow.generic.max_size.memory_size = 400 * 1024 * 1024;
    // pace2_config.tracking.subscriber.generic.max_size.memory_size = 400 * 1024 * 1024;

    if (options.full_features) {
        printf("Enabling full classification and decoding feature set.\n\n");

        // Stage 1: enable IP defragmentation 
        pace2_config.s1_preparing.defrag.enabled = 1;
        pace2_config.s1_preparing.max_framing_depth = 10;
        pace2_config.s1_preparing.max_decaps_level = 10;

        // Stage 2: enable PARO and set necessary values 
        pace2_config.s2_reordering.enabled = 1;
        pace2_config.s2_reordering.packet_buffer_size = 16 * 1024 * 1024;
        pace2_config.s2_reordering.packet_timeout = 5 * pace2_config.general.clock_ticks_per_second;

        // Stage 3: enable specific classification components 
        pace2_config.s3_classification.asym_detection_enabled = 1;
        pace2_config.s3_classification.sit.enabled = 1;
        pace2_config.s3_classification.sit.key_reduce_factor = 1;
        pace2_config.s3_classification.sit.memory = 1 * 1024 * 1024;
        pace2_config.s3_classification.cdn_caching_enabled = 1;
        pace2_config.s3_classification.os_enabled = 1;
        pace2_config.s3_classification.nat_enabled = 1;
        pace2_config.s3_classification.rtp_performance_enabled = 1;
        pace2_config.s3_classification.csi_enabled = 1;

        // Stage 4: enable decoding 
        pace2_config.s4_decoding.enabled = 1;
        pace2_config.general.event_correlation.enable_event_correlation = 1;
    } 
    else 
    {
        printf("Using minimal feature set.\n\n");
    }

    /* uncomment the following line to only activate classification of protocol HTTP */
    // PACE2_PROTOCOLS_BITMASK_RESET(config.s3_classification.enabled_classifications.protocols.bitmask);
    // config.s3_classification.enabled_classifications.protocols.protocols.http = IPQ_TRUE;

    /* uncomment the following line to only activate decoding of protocol HTTP */
    // PACE2_PROTOCOLS_BITMASK_RESET(pace2_config.s4_decoding.active_advanced_decoders.bitmask);
    // pace2_config.s4_decoding.active_advanced_decoders.protocols.http = IPQ_TRUE;

    // Initialize PACE 2 detection module 
    pace2_init( &pace2_config, &pace2 );

    if ( pace2 == NULL ) 
    {
        fprintf(stderr, "Initialization of PACE module failed\n" );
        exit(1);
    }

    // Licensing 
    if ( license_file != NULL ) 
    {

        const ipoque_pace_license_information_t *lic_info = NULL;

        switch( pace2_get_license_information( pace2, &lic_info, 0 ) ) 
	{
            case PACE2_SUCCESS:
                fprintf(stderr, "License init error code: %u\n"
                        "License init error reason: %s\nLicense load error code: %u\n"
                        "License load error reason: %s\nLicense validation error code: %u\n"
                        "License validation error reason: %s\nLicense limitation error code: %u\n"
                        "License limitation error reason: %s\nNumber of mac addresses found: %u\n",
                        lic_info->init_error_code, lic_info->init_error_reason,
                        lic_info->load_error_code, lic_info->load_error_reason,
                        lic_info->validation_error_code, lic_info->validation_error_reason,
                        lic_info->limitation_error_code, lic_info->limitation_error_reason,
                        lic_info->no_of_mac_addresses_found);
                break;

            case PACE2_PARAM_MISSING:
                fprintf( stderr, "Parameter missing\n" );
                break;

            case PACE2_FAILURE:
                fprintf( stderr, "Licensing feature is not available.\n\n" );
                break;
            default:
                break;
        }
    }
} // pace_configure_and_initialize 
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
static void pace_cleanup_and_exit( void )
{
  
    // Flush any remaining packets from the buffers 
    pace2_flush_engine( pace2, 0 );

    // Process packets which are ejected after flushing 
    stage3_to_5();

    // Output detection results 
    pace_print_results();

    // Destroy PACE 2 module and free memory 
    pace2_exit_module( pace2 );
    
} // pace_cleanup_and_exit 
//-----------------------------------------------------------------------------------
int main( int argc, char **argv )
{
    const char * license_file = NULL;
    int rc;
    
    // Initialize dpdk 
    rc = dpdk_init(argc, argv);
    if (rc < 0) 
    {
    	fprintf(stderr, "Initializing of DPDK failed!\n" );
    	return 1;
    }

    argc -= rc;
    argv += rc;

    int c = 0;
    while ((c = getopt(argc, argv, "ahpP:l:o:")) != -1) 
    {
        switch (c) 
	{
            case 'h':
                printf("Usage: test1 [dpdk options] -- [options]\n\n"
                        "  -a\tEnable full PACE feature set.\n"
                        "  -p\tWhat port should be captured on.\n"
                        "  -P\tEnable promiscuous mode"
                        "  -l\tUse a specific license file for PACE.\n"
                        "  -n\tNumber of packets, that should be captured before PACE statistic is printed.\n"
                        "  -h\tPrint this help message\n\n");
                return 0;
            case 'a':
                options.full_features = 1;
                break;
            case 'p':
                options.port = strtoul(optarg, NULL, 10);
                break;
            case 'n':
                options.print_stats_after_x_packets = strtoul(optarg, NULL, 10);
                break;
            case 'l':
                license_file = optarg;
                break;
            case 'P':
                options.promiscuous_mode = 1;
                break;
        }
    }
    
    // Initialize PACE 2 
    pace_configure_and_initialize( license_file );


    // Read from  file and pass packets to stage1_and_2 
    rc = packet_loop( &stage1_and_2 );
    if ( rc != 0 ) 
    {
        fprintf(stderr, "Error in packet loop!\n" );
        return 1;
    }

    pace_cleanup_and_exit();

    dpdk_finish();

    return 0;
}