ifeq ($(RTE_SDK),)
$(error "Please define RTE_SDK environment variable")
endif

ifeq ($(PACE2_SDK),)
$(error "Please define PACE2_SDK environment variable")
endif

# Default target, can be overriden by command line or environment
RTE_TARGET ?= x86_64-native-linuxapp-gcc

include $(RTE_SDK)/mk/rte.vars.mk

# binary name
APP = test1

# all source are stored in SRCS-y
SRCS-y := main.c


PACE2_INCLUDES = -I$(shell readlink -f $(PACE2_SDK)/include/ipoque)
PACE2_LIBRARY = $(shell readlink -f $(PACE2_SDK)/lib)

CFLAGS += -g 
CFLAGS += $(PACE2_INCLUDES)
CFLAGS += $(WERROR_FLAGS)

LDFLAGS += -L$(PACE2_LIBRARY) -lipoque_pace2 -lz

include $(RTE_SDK)/mk/rte.extapp.mk 
